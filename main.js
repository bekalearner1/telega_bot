const TelegramApi = require('node-telegram-bot-api');
const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

const data = {
    startMessage: '*Здравствуй Милани* ☺️\n\nМеня зовут *Милби*😺, творение твоего супруга, чтоб ты не грустила, и была всегда на позитиве.\n\n*- Могу в зависимости от настроения, передать послание твоего супруга😸\n\n- Могу одаривать комплементами😽\n\n- Могу мотивировать и шутить 😸\n\n- Могу предложить тебе песни для успокоения души 😸\n\n- А также поделиться парочкой секретов твоего супруга* 😼\n\nБуду всегда рад угодить тебе. Если я тебе понравлюсь, не забудь похвалить меня ❤️\n\nЧтобы получить список команд, нажми сюда 👉 /commands',
    complimentMessage: [],
    motivationMessage: [],
    poetryMessage: [],
    musicMessage: [
        'https://www.youtube.com/watch?v=BOUpNtk5Spk',
        'https://www.youtube.com/watch?v=0b5uulBrDrs',
        'https://www.youtube.com/watch?v=JRQbVNzmCK0',
        'https://www.youtube.com/watch?v=MPlkHxFA-Qg',
        'https://www.youtube.com/watch?v=vByQ7TtcE-Y',
        'https://www.youtube.com/watch?v=MwpMEbgC7DA',
        'https://www.youtube.com/watch?v=MwpMEbgC7DA',
        'https://www.youtube.com/watch?v=25ROFXjoaAU',
        'https://www.youtube.com/watch?v=25ROFXjoaAU',
        'https://www.youtube.com/watch?v=wYEB9ntsaxs',
        'https://www.youtube.com/watch?v=YtkhHOkXj3I',
        'https://www.youtube.com/watch?v=p8NXn1PknkM',
        'https://www.youtube.com/watch?v=jYlZ8SNU_Go',
        'https://www.youtube.com/watch?v=Y3AW7leprn8',
        'https://www.youtube.com/watch?v=PDiERJGyi_E',
        'https://www.youtube.com/watch?v=PDiERJGyi_E',
        'https://www.youtube.com/watch?v=0lZ2AFZR6IU',
        'https://www.youtube.com/watch?v=xizN47Box_Y',
        'https://www.youtube.com/watch?v=mo6sujHsXPk',
        'https://www.youtube.com/watch?v=bPs0xFd4skY',
        'https://www.youtube.com/watch?v=jb-i_ARlumQ',
        'https://www.youtube.com/watch?v=jCw9WiEkhfI',
        'https://www.youtube.com/watch?v=--eH76tgoNw',
        'https://www.youtube.com/watch?v=hT_nvWreIhg',
        'https://www.youtube.com/watch?v=WDIY6ybkTh8',
        'https://www.youtube.com/watch?v=rF5oITEq43Q',
        'https://www.youtube.com/watch?v=FdMuFjzCwc0',
        'https://www.youtube.com/watch?v=3tQIwR17ufo',
        'https://www.youtube.com/watch?v=ho1RzYneMtM',
        'https://www.youtube.com/watch?v=jLNrvmXboj8',
        'https://www.youtube.com/watch?v=DGn3Ic8XC8M',
        'https://www.youtube.com/watch?v=HJrKVYJdABc',
        'https://www.youtube.com/watch?v=FM7MFYoylVs',
        'https://www.youtube.com/watch?v=edzt82nC45k',
        'https://www.youtube.com/watch?v=mODNerMYODc',
        'https://www.youtube.com/watch?v=J6enOG547lk',
        'https://www.youtube.com/watch?v=A3TKxux17ZA',
        'https://www.youtube.com/watch?v=KIAZWfSmNOU',
        'https://www.youtube.com/watch?v=lE-GhpoL3c4',
        'https://www.youtube.com/watch?v=ZPyyVdK-HeI',
        'https://www.youtube.com/watch?v=39F6sWLeETs',
        'https://www.youtube.com/watch?v=jsM-36vPgpQ',
        'https://www.youtube.com/watch?v=3o4ug07qJBI',
        'https://www.youtube.com/watch?v=XwxLwG2_Sxk',
        'https://www.youtube.com/watch?v=UN4VLmo1QG4',
        'https://www.youtube.com/watch?v=eqJ4LLosGJE',  
],
    secretMessage: 'Здравствуй еще раз *Милани*, я храню в себе три секрета 🙀\n\nЗнаю не густо, но имеем что имеем 😸\n\nОткрыть вы их сможете в полночь с *18го октября.*\n\n*Выбери ниже, какой из трех тебе раскрыть 😼*',
    messageMessage: '*Я так полагаю пришло время услышать послания. Я так ждал этого момента 😽*\n\nТолько прежде чем открывать. Ты должна знать.\n\nМой создатель вложил в это всю душу. Он создал меня для того чтобы тебе не было одиноко, грустно или тоскливо. Хотел, чтобы ты была стойкой и терпеливой в этот нелегкий период. Знай в скором времени вернется твой мужчина, тот, кто с Божьей помощью, восполнит всю твою чашу счастья и  любви . А до этих пор береги себя и будь спокойна 😽',
    getComplimentMessage: function(link) {
            axios.get(link).then( (html) => {
                const $ = cheerio.load(html.data);
                $('div.stickynote p').each((i, elem) => {
                    this.complimentMessage[(this.complimentMessage.length - 1) + 1] = `${$(elem).text()}`;
                });
            });
        },
    getMotivationMessage: function(link) {
        axios.get(link).then( (html) => {
            const $ = cheerio.load(html.data);
            $('#main_content_left > div > p:nth-child(1)').each((i, elem) => {
                this.motivationMessage[(this.motivationMessage.length - 1) + 1] = `${$(elem).text()}`;
            });
        });
    },
    getPoetryMessage: function(link) {
        axios.get(link).then( (html) => {
            const $ = cheerio.load(html.data);
            $('.stih').each((i, elem) => {
                this.poetryMessage[(this.poetryMessage.length - 1) + 1] = `${$(elem).text()}`;
            });
        });
    }
};


data.getComplimentMessage('https://statusycitaty.ru/komplimentyi/samyie-krasivyie-komplimentyi-devushke.html');
data.getComplimentMessage('https://statusycitaty.ru/komplimentyi/sms-komplimentyi-devushke.html');
data.getComplimentMessage('https://statusycitaty.ru/komplimentyi/prikolnyie-komplimentyi-devushke.html');
data.getMotivationMessage('https://allcitations.ru/tema/schaste');
data.getMotivationMessage('https://allcitations.ru/tema/citaty-velikix-lyudej');
data.getPoetryMessage('https://otmetim.info/stixi-o-lyubvi-devushke/');

const { startMessage, complimentMessage, motivationMessage, poetryMessage, musicMessage, secretMessage, messageMessage } = data;

const accessDate = new Date('Oct 18 2022 00:00:00 GMT+0600');
const currentDate = new Date();

const chatBeka = 453988967;
const users = [5473661045, 453988967, 5421252297];

const token = '5626053973:AAEy9ooV35Ck_D0Vr0zTBXAp_dhBekc74bE';
const bot = new TelegramApi(token, {polling: true});
const commands = [

    {command: '/compliment', description: 'Одари комплементами :)'},
    {command: '/poetry', description: 'Расскажи стишок :)'},
    {command: '/motivation', description: 'Немного мудрости!'},
    {command: '/song', description: `Предложи песню`},
    {command: '/message', description: 'Получить послание'},
    {command: '/secret', description: `Какой секрет ты хранишь ?`},
    {command: '/info', description: 'Узнать о боте'},
    {command: '/commands', description: 'Узнать команды'}
];
const secretOptions = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: 'Первый секрет', callback_data: '1'}],
            [{text: 'Второй секрет', callback_data: '2'}],
            [{text: 'Третий секрет', callback_data: '3'}]
        ],
    }),
    parse_mode: 'Markdown'
};
const messageOptions = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: 'Открой когда станет грустно', callback_data: 'sad'}],
            [{text: 'Открой если соскучилась', callback_data: 'miss'}],
            [{text: 'Открой если ты злишься на меня', callback_data: 'angry'}],
            [{text: 'Открой если заболела', callback_data: 'sick'}],
            [{text: 'Открой как пойдет дождь.', callback_data: 'rain'}],
            [{text: 'Открой как пойдет первый снег', callback_data: 'snow'}],
            [{text: 'Открой когда будешь ревновать', callback_data: 'jealous'}],
            [{text: 'Открой когда будешь меня вспоминать', callback_data: 'remember'}],
            [{text: 'Открой когда пойдешь ложиться спать', callback_data: 'sleep'}],
            [{text: 'Открой когда придёт время нашей свадьбы', callback_data: 'wedding'}]
        ],
    }),
    parse_mode: 'Markdown'
};
const thanksOptions = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: 'Спасибо Милби ❤️', callback_data: 'like'}],
        ],
    }),
    parse_mode: 'Markdown'
};
//-----------------------------------------------------------------------------------------------------------
const getCommands = function(){
    let listOfCommands = '';
    commands.forEach(i => {
        listOfCommands += `\n${i.description} - ${i.command}\n`;
    });

    return listOfCommands;
};
//------------------------------------------------------------------------------------------------------------

const start = function(){
    bot.setMyCommands(commands);
    bot.on('message', async msg => {

        const opts = { parse_mode: 'Markdown'};
        const text = msg.text;
        const chatId = msg.chat.id;
        if(Boolean(msg.text) === true){
            let userInfo = '';
            if( Boolean(msg.chat) === true && Boolean(msg.chat.id) === true){
                userInfo += `Chat ID - ${msg.chat.id}\n`;
            }
            if( Boolean(msg.chat) === true && Boolean(msg.chat.username) === true){
                userInfo += `Username - ${msg.chat.username}\n`;
            }
            if( Boolean(msg.chat) === true && Boolean(msg.chat.first_name) === true){
                userInfo += `First name - ${msg.chat.first_name}\n`;
            }
            if( Boolean(msg.chat) === true && Boolean(msg.chat.last_name) === true){
                userInfo += `Last name - ${msg.chat.last_name}\n`;
            }
            userInfo += `Text - ${text}`;
            await bot.sendMessage(chatBeka, userInfo);
        }
        
        if(chatId === 5473661045){
            users.forEach( i => {
                bot.sendMessage(i, text);   
            });
        }
        if(chatId === 453988967 || chatId === 5473661045 || chatId === 5421252297){
            if( text === '/start' || text === '/info'){
                await bot.sendSticker(chatId, 'https://tlgrm.ru/_/stickers/ab4/d1a/ab4d1a6b-1225-30cc-b747-2497eefb6ced/4.webp');
                return bot.sendMessage(chatId, `${startMessage}`, opts);
            }
            if( text === '/commands'){
                return bot.sendMessage(chatId, `*Мой список команд и кодовых слов:*\n ${getCommands()}`, opts);
            }
            if( text === '/compliment'){
                let randomNumber = Math.round(Math.random() * complimentMessage.length);
                await bot.sendSticker(chatId, 'https://tlgrm.ru/_/stickers/c07/6b9/c076b9ea-9aa1-36ed-8187-04466f3b00ba/192/52.webp');
                return bot.sendMessage(chatId, `*${complimentMessage[randomNumber]}*😽`, thanksOptions);
            }
            if( text === '/motivation'){
                let randomNumber = Math.round(Math.random() * (motivationMessage.length - 1));
                await bot.sendPhoto(chatId, 'https://st2.depositphotos.com/3038789/5811/i/600/depositphotos_58117015-stock-photo-hand-drawn-chalkboard-sign-dream.jpg');
                return bot.sendMessage(chatId, `*${motivationMessage[randomNumber]}*😸`, thanksOptions);
            }
            if( text === '/song'){
                let randomNumber = Math.round(Math.random() * (musicMessage.length - 1));
                await bot.sendSticker(chatId, `https://tlgrm.ru/_/stickers/ab4/d1a/ab4d1a6b-1225-30cc-b747-2497eefb6ced/7.webp`);
                return bot.sendMessage(chatId, `*${musicMessage[randomNumber]}\n\nПожалуйста, слушайте и наслаждайтесь 😸*`, thanksOptions);
            }
            if( text === '/poetry'){
                let randomNumber = Math.round(Math.random() * (poetryMessage.length - 1));
                await bot.sendSticker(chatId, `https://tlgrm.ru/_/stickers/c07/6b9/c076b9ea-9aa1-36ed-8187-04466f3b00ba/5.webp`);
                return bot.sendMessage(chatId, `${poetryMessage[randomNumber].slice((this.length - 1), -11)}`, thanksOptions);
            }
            if( text === '/secret'){
                await bot.sendSticker(chatId, 'https://tlgrm.ru/_/stickers/06c/d14/06cd1435-9376-40d1-b196-097f5c30515c/192/14.webp');
                return bot.sendMessage(chatId, `${secretMessage}`, secretOptions);
            }
            if( text === '/message'){
                await bot.sendSticker(chatId, 'https://selcdn.tlgrm.app/stickers/06c/d14/06cd1435-9376-40d1-b196-097f5c30515c/192/4.webp');
                return bot.sendMessage(chatId, `${messageMessage}`, messageOptions);
            }
            else{
                return bot.sendMessage(chatId, 'Такой команды не сущевствует 😸\n\nУзнать комманды -> /commands');
            }
        }else{
            return bot.sendMessage(chatId, 'Замечена странная активность. \n\nCокращена максимальное количество пользователей');
        }
    });

    bot.on('callback_query', async msg =>{
        const data = msg.data;
        const chatId = msg.message.chat.id;
        const audioFiles = {
            sad: fs.createReadStream('./audio/sad.ogg'),
            miss: fs.createReadStream('./audio/miss.ogg'),
            sick: fs.createReadStream('./audio/sick.ogg'),
            angry: fs.createReadStream('./audio/angry.ogg'),
            rain: fs.createReadStream('./audio/rain.ogg'),
            snow: fs.createReadStream('./audio/snow.ogg'),
            jealous: fs.createReadStream('./audio/jealous.ogg'),
            remember: fs.createReadStream('./audio/remember.ogg'),
            sleep: fs.createReadStream('./audio/sleep.ogg'),
        };

        const {sad, miss, sick, angry, rain, snow, jealous, remember, sleep} = audioFiles;

        if (chatId !== chatBeka){
            if(data === 'sad'){
                await bot.sendMessage(chatBeka, 'sad opened');
                await bot.sendMessage(chatId, '*Милани, не грусти пожалуйста 😿*\n\nЗнай, после заката всегда наступает рассвет😸 Все трудности это только испытания бога, которые нужно пройти, только и всего😸 За эти испытания ты без спора получишь награду. Не забывай об этом.\n\nБоженька тебе поможет со всем, главное не забывай о нем и делай ДУА 😸 Не решаемых проблем не бывает😽\n\n*Надеюсь послание поможет вам 😽👇*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, sad);
            }
            if(data === 'miss'){
                await bot.sendMessage(chatBeka, 'miss opened');
                await bot.sendMessage(chatId, '*Оу, соскучилась по супругу, как мило😻*\n\nЯ порекомендую сказать ему это лично. Уверен, он порадуется. Точнее он комнату вверх дном перевернет от счастья 😹\n\n*Надеюсь послание согреет тебя теплом 😽👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, miss);
            }
            if(data === 'sick'){
                await bot.sendMessage(chatBeka, 'sick opened');
                await bot.sendMessage(chatId, '*Милани, выздоравливай поскорей😿*\n\n Порадую тебя тем фактом, что твои грехи смываются рекой. В этом есть благо Божье 😸 \nХорошо следи за собой, не перетруждай себя и просто отдыхай. Пей лекарства вовремя 😾\n\n*Ниже послание муженька, надеюсь это как то облегчит твое состояние 😾👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, sick);
            }
            if(data === 'angry'){
                await bot.sendMessage(chatBeka, 'angry opened');
                await bot.sendMessage(chatId, '*Не злись на него 😾 Он прекрасный человек.* \n\nДа, творит глупости временами. Но, на то вы и люди, а не ангелы. Самый лучший вариант действий в таком случае это, простить и забыть в течении 3 дней😸\nЯ надеюсь он не сделал настолько плохую вещь, что заслужил твой гнев😾\n\n*Ниже его послание по такому случаю😸👇*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, angry);
            }
            if(data === 'rain'){
                await bot.sendMessage(chatBeka, 'rain opened');
                await bot.sendMessage(chatId, '*Дождик пошел.* Это значит, ангелы спускаются с небес 😸 Чудесно, неправда ли?\n\nХорошенько помолись, за всех родных и близких😽\n\n*Мокрое послание мчится в ваш чат 😹*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, rain);
            }
            if(data === 'snow'){
                await bot.sendMessage(chatBeka, 'snow opened');
                await bot.sendMessage(chatId, '*Первый снег, эххх. Так романтично 😻*\n\nПоздравляю тебя с первым снегом. Зима стучится в двери, принося с собой слякоть, лужи и грязь мъа 😾\nЗнаю юмор от бога 😹\n\n*С солнечной Малайзии, летит теплое письмо 😸👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, snow);
            }
            if(data === 'jealous'){
                await bot.sendMessage(chatBeka, 'jealous opened');
                await bot.sendMessage(chatId, '*Ревность сильная эмоция😼*\n\nТы ревнуешь?\nИнтересно к чему, к компьютеру? 😹 \nПоверь мне, тебе не о чем волноваться. Он настолько занят развитием, что даже поспать толком не получается😸\nТак что, не волнуйся. Он не глупый парень, и такую как ты, ни на что на свете не променяет😻\n\n*Я думаю он сможет тебя успокоить 😸👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, jealous);
            }
            if(data === 'remember'){
                await bot.sendMessage(chatBeka, 'remember opened');
                await bot.sendMessage(chatId, '*Хорошие воспоминания это круто 😸*\n\nМеня тоже вспоминай почаще😾 Договорились?\n\n*Послание ждет тебя 😸👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, remember);
            }
            if(data === 'sleep'){
                await bot.sendMessage(chatBeka, 'sleep opened');
                await bot.sendMessage(chatId, '*Сладких снов Милани 😸*\n\nПусть тебе приснится то, чего ты желаешь больше всего на свете. Пусть ангелы оберегают тебя во время сна, и кошмары обходят стороной 😽\n\n*Послание на ночь от супруга 😽👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendAudio(chatId, sleep);
            }
            if(data === 'wedding'){
                await bot.sendMessage(chatBeka, 'wedding opened');
                await bot.sendMessage(chatId, '*Поздравляю вас с бракосочетанием 😽*\n\nЕсли конечно, ты не открыла послание раньше😼\n\nПусть брак ваш будет примером для многих. Пусть минуют вас сложные испытания. Проживите счастливую жизнь вместе, всегда помогая и поддерживая друг друга. И встретьтесь вновь в раю Фирдаус 😽 Амин \n\n*Время пришло 😻👇🏻*', {parse_mode: 'Markdown'});
                return bot.sendMessage(chatId, 'Подойди ко мне в этот день, и шепни на ушко "Мы смогли".');
            }
            if(data === '1'){
                if(currentDate >= accessDate){
                    await bot.sendMessage(chatBeka, '1 secret opened');
                    return bot.sendMessage(chatId, '*Мечтает когда-нибудь прыгнуть с парашутом, из самолета 😺*', {parse_mode: 'Markdown'});
                }
            }
            if(data === '2'){
                if(currentDate >= accessDate){
                    await bot.sendMessage(chatBeka, '2 secret opened');
                    return bot.sendMessage(chatId, '*Одна из его целей, выучить весь священный Коран. Учит по немногу 😸*', {parse_mode: 'Markdown'});
                }
            }
            if(data === '3'){
                if(currentDate >= accessDate){
                    await bot.sendMessage(chatBeka, '3 secret opened');
                    return bot.sendMessage(chatId, '*Боится умереть в одиночестве, и что об этом никто не узнает 😿*', {parse_mode: 'Markdown'});
                }
            }
            if(data === 'like'){
                await bot.sendSticker(chatId, 'https://tlgrm.ru/_/stickers/c07/6b9/c076b9ea-9aa1-36ed-8187-04466f3b00ba/1.webp');
                return bot.sendMessage(chatId, '❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️\n* Всегда пожалуйста Милани 😻*', {parse_mode: 'Markdown'});
            }
        }
    });
};


start();

const botSendPost = () => {
    const autoMessages = {
        morningMessages: ['Пусть будет утро добрым,\nПусть будет день прекрасным!\nПусть будет настроение\nВеселым, светлым, ясным! 😸','Пусть будет день чудесным,\nКак твои глаза.\nЯрким, добрым, интересным,\nКак и ты сама 😸','«Утро доброе!» ― желаю,\nПредлагаю выпить чаю.\nУлыбнитесь, Вам идет,\nПусть отлично день пройдет! 😻', 'Утра доброго желаю,\nЛучик счастья посылаю,\nБед сегодня не встречать\nИ легко все успевать! ❤️', 'С утром добрым, улыбнись!\nПосмотри — прекрасна жизнь.\nПусть тебя ждет день чудесный,\nРадостный и интересный.','В небе утро расцветает,\nСловно сказка оживает.\nПусть волшебно день пройдёт,\nМного счастья принесёт!', 'Желаю с бодростью проснутся,\nГрядущим планам улыбнуться,\nУдачу в компаньоны взять,\nИ утро новое начать. ❤️'],
        nightMessages: ['Мы устали очень-очень...\nСладкой и спокойной ночи,\nСнов волшебных, откровенных,\nЧувств счастливых и бесценных! 😻', 'И снова вечер гасит свечи,\nИ нам пора ложиться спать.\nСпокойной ночи, спи, до встречи,\nЕе я очень буду ждать. 😻', 'Прекрасных снов тебе желаю.\nПусть все заботы сгинут прочь!\nПусть исцеляющей и доброй\nСегодня будет фея-ночь. 😻', 'Доброй ночи, добрых снов,\nА во снах — волшебных слов\nОт советчиков небесных,\nОт волшебников чудесных! 😻', 'День суеты окончен.\nЖелаю крепко спать.\nЖелаю снов не видеть,\nА просто отдыхать.\n\nПусть свежим будет воздух,\nПрохлады чтоб вдохнуть.\nСпокойной доброй ночи!\nПриятно отдохнуть! 😻', 'День прошел и ночь в окошко заглянула,\nЧтоб твоя усталость ускользнула.\nПусть ночь твои заботы успокоит\nИ сердце ничего не беспокоит. 😻', 'Пусть приснится тебе сон цветной,\nИ Ангел-хранитель будет с тобой,\nИ будет постель удобной очень,\nХочу пожелать спокойной ночи. 😻'],
    };
    const {morningMessages, nightMessages} = autoMessages;
    const templatePhraseMorning = 'Порекомендую тебе песню на сегодня, и конечно же, не забываем про мотивацию 😹';
    const templatePhraseNight = 'Не забудь сделать дуа  и почистить зубы перед сном 😸\n\n*Дуа перед сном: Аллохума бисмика амууту ва ахъя.*';
    const morningMessageDate = new Date('Oct 18 2022 07:00:01 GMT+0600');
    const nightMessageDate = new Date('Oct 18 2022 23:00:01 GMT+0600');
    const complimentMessageDate = new Date('Oct 18 2022 14:20:01 GMT+0600');
    const poetryMessageDate = new Date('Oct 18 2022 18:40:01 GMT+0600');
    const complimentTime = new Date('Oct 18 2022 21:20:01 GMT+0600');
    setInterval(async() => {
       let currentDate = new Date();
       if(morningMessageDate.getHours() === currentDate.getHours() && morningMessageDate.getMinutes() === currentDate.getMinutes() && morningMessageDate.getSeconds() === currentDate.getSeconds()){
            users.forEach(async (item) => {
                let randomNumber = Math.round(Math.random() * (morningMessages.length - 1));
                let randomNumber2 =  Math.round(Math.random() * (musicMessage.length - 1));
                let randomNumber3 = Math.round(Math.random() * (motivationMessage.length - 1));
                await bot.sendMessage(item, `*Проснись и пой Милани 😽\n\n${morningMessages[randomNumber]}*\n\n${templatePhraseMorning}\n\n*Мотивация на день😽:*\n${motivationMessage[randomNumber3]}\n\n*Твоя песня на сегодня😻:*\n${musicMessage[randomNumber2]}`, {parse_mode: 'Markdown'});
            });
        }
        if(nightMessageDate.getHours() === currentDate.getHours() && nightMessageDate.getMinutes() === currentDate.getMinutes() && nightMessageDate.getSeconds() === currentDate.getSeconds()){
            users.forEach(async (item) => {
                let randomNumber = Math.round(Math.random() * (nightMessages.length - 1)); 
                await bot.sendMessage(item, `Милани, не задерживайся до поздна. Ложись спать пораньше😽\n\n*${nightMessages[randomNumber]}*\n\n${templatePhraseNight}`, {parse_mode: 'Markdown'});
            });
        }
        if(complimentTime.getHours() === currentDate.getHours() && complimentTime.getMinutes() === currentDate.getMinutes() && complimentTime.getSeconds() === currentDate.getSeconds()){
            users.forEach(async (item) => {
                let randomNumber = Math.round(Math.random() * (complimentMessage.length - 1)); 
                await bot.sendSticker(item, 'https://tlgrm.ru/_/stickers/c07/6b9/c076b9ea-9aa1-36ed-8187-04466f3b00ba/192/52.webp');
                await bot.sendMessage(item, `*Вечерняя порция сладких слов 😻\nПринимай 😽*\n\n${complimentMessage[randomNumber]} ❤️❤️❤️`, {parse_mode: 'Markdown'});
            });
        }
        if(complimentMessageDate.getHours() === currentDate.getHours() && complimentMessageDate.getMinutes() === currentDate.getMinutes() && complimentMessageDate.getSeconds() === currentDate.getSeconds()){
            users.forEach(async (item) => {
                let randomNumber = Math.round(Math.random() * (complimentMessage.length - 1)); 
                await bot.sendSticker(item, 'https://tlgrm.ru/_/stickers/c07/6b9/c076b9ea-9aa1-36ed-8187-04466f3b00ba/192/52.webp');
                await bot.sendMessage(item, `*Комплименты заказывали 😹? *\n\n${complimentMessage[randomNumber]} ❤️❤️❤️`, {parse_mode: 'Markdown'});
            });
        }
        if(poetryMessageDate.getHours() === currentDate.getHours() && poetryMessageDate.getMinutes() === currentDate.getMinutes() && poetryMessageDate.getSeconds() === currentDate.getSeconds()){
            users.forEach(async (item) => {
                let randomNumber = Math.round(Math.random() * (poetryMessage.length - 1)); 
                await bot.sendSticker(item, `https://tlgrm.ru/_/stickers/c07/6b9/c076b9ea-9aa1-36ed-8187-04466f3b00ba/5.webp`);
                await bot.sendMessage(item, `*Пришло время рассказать стих 😻*\n\n${poetryMessage[randomNumber].slice((this.length - 1), -11)} ❤️❤️❤️`, {parse_mode: 'Markdown'});
            });
        }
    }, 1000);
};

botSendPost();